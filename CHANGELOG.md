# Change Log

All notable changes to the "koboldcomplete" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [v1.0.0]

- Added experimental inline completion for code
- Added a snippet for ChatML bare request
- Added support for Llama models
- Improved error handling and increased default timeout in settings
- Added new default template for llama autocomplete
- Added support for bare requests, snippets for Llama and Mistral instruct
- Added stop command

## [Unreleased]

- Initial release