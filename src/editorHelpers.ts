import * as vscode from 'vscode';

/**
 * Helper function to return selectedText or current line before cursor
 * selectedText is all selected text or current line text before cursor
 * lineNumber is line of the cursor or line of the last selected symbol
 * @param editor current editor
 * @returns {lineNumber: number, selectedText: string}
 */
export function getSelection(editor: vscode.TextEditor) {
    let lineNumber = editor.selection.active.line;
    // If selected 
    let selectedText = editor.document.getText(
        new vscode.Range(
            editor.selection.start,
            editor.selection.end
        )
    );

    // If no selected chars send current line
    if (!selectedText) {
        selectedText = editor.document.getText(
            new vscode.Range(
                new vscode.Position(lineNumber, 0),
                editor.selection.active
            )
        );
    } else {
        // To return last line in the selection
        lineNumber = editor.selection.end.line;
    }

    return { lineNumber, selectedText };
}

/**
 * Inserts text below line
 * @param editor 
 * @param line 
 * @param text 
 */
export function pasteTextBelow(editor: vscode.TextEditor, line: number, text: string) {
    const endOfTheLine = editor.document.lineAt(line).range.end;
    editor.edit((edit) => {
        edit.insert(endOfTheLine, '\n' + text);
    });
}