let genkey = '';

/**
 * Generate and store new key
 * @returns genkey
 */
export function generateAndStoreKey(): string {
    genkey = "KCPPVSC" + (Math.floor(1000 + Math.random() * 9000)).toString(); // Random multiuser key for generation
    console.log('Genkey: ', genkey);
    return genkey;
}

/**
 * Get stored key
 * @returns genkey
 */
export function getKey(): string {
    if (!genkey) {
        generateAndStoreKey();
    }
    console.log('Genkey: ', genkey);
    return genkey;
}