import * as vscode from 'vscode';
import fetch from 'node-fetch';
import { getKey, generateAndStoreKey } from './keyManager';

/**
 * Request to KoboldCPP to generate
 * @param prompt generation prompt
 * @returns 
 */
export async function generate(prompt: string) {
    const params = vscode.workspace.getConfiguration('koboldcomplete');
    const temperature = params.temperature;
    const topP = params.top_p;
    const maxLength = params.max_length;
    const timeout = params.timeout * 1000;
    const host = params.host;
    const genkey = generateAndStoreKey();
    let result = '';


    try {
        // Timeout signal
        const controller = new AbortController();
        const timeoutId = setTimeout(async () => {
            controller.abort();
        }, timeout);
        const signal = controller.signal;

        const response = await fetch(`${host}/api/v1/generate`, {
            signal,
            method: 'POST',
            headers: {
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                genkey,
                prompt,
                temperature,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                top_p: topP,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                max_length: maxLength
            })
        });
        clearTimeout(timeoutId);

        const responseData = await response.json();
        result = responseData.results[0].text;
    } catch (exception: any) {
        console.log('Generate excepion', exception);
        if (exception?.name === 'AbortError') {
            await abort(); // Send request to LoboldCPP to abort generation
            vscode.window.showErrorMessage('KoboldComplete: Connection timeout, try to set timeout in settings to higher number');
            return result;
        }
        if (exception?.cause?.code === 'ECONNREFUSED') {
            vscode.window.showErrorMessage("KoboldComplete: Couldn't establish connection check hostname in KoboldComplete settings");
            return result;
        }
        vscode.window.showErrorMessage("KoboldComplete: Couldn't establish connection with KoboldCPP " + exception);
    }
    return result;
}

/**
 * Request to KoboldCPP to stop generation
 * @returns null
 */
export async function abort() {
    const params = vscode.workspace.getConfiguration('koboldcomplete');
    const timeout = 5 * 1000; // @todo think about timeout on abort
    const host = params.host;

    try {

        // timeout signal
        const controller = new AbortController();
        const timeoutId = setTimeout(async () => {
            controller.abort();
        }, timeout);
        const signal = controller.signal;
        const genkey = getKey();

        const response = await fetch(`${host}/api/extra/abort`, {
            signal,
            method: 'POST',
            headers: {
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                genkey
            })
        });
        clearTimeout(timeoutId);

        const responseData = await response.json();
        if (responseData?.success !== 'true') {
            vscode.window.showErrorMessage("KoboldComplete: Can't abort generation check KoboldCPP console for more info");
        }
    } catch (exception) {
        console.log('Abort excepion ', exception);
        vscode.window.showErrorMessage("KoboldComplete: Couldn't establish connection with KoboldCPP. " + exception);
    }
    return null;
}