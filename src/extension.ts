import * as vscode from 'vscode';
import {stop, autocomplete, question, bare, getLastCompletion, deleteLastCompletion} from './commands';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let inlineCompletionItemProvider = vscode.languages.registerInlineCompletionItemProvider('*', {
        provideInlineCompletionItems(document, position, context, token) {
            if (context.triggerKind === 0) {
                let completionItem = new vscode.InlineCompletionItem(getLastCompletion());
                completionItem.filterText = document.getText(new vscode.Range(
                    new vscode.Position(position.line, 0),
                    position
                ));
                completionItem.insertText = completionItem.filterText + completionItem.insertText;
                deleteLastCompletion();
                return [completionItem];
            }
            return [];
        },
    });
    context.subscriptions.push(
        vscode.commands.registerCommand('koboldcomplete.stop', stop),
        vscode.commands.registerTextEditorCommand('koboldcomplete.complete', autocomplete),
        vscode.commands.registerTextEditorCommand('koboldcomplete.question', question),
        vscode.commands.registerTextEditorCommand('koboldcomplete.bare', bare),
        inlineCompletionItemProvider,
    );
}

export function deactivate() { }
