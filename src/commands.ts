import * as vscode from 'vscode';
import { generate, abort } from './requests';
import { getSelection, pasteTextBelow } from './editorHelpers';
import { getKey } from './keyManager';
import getTemplate, { COMMAND_AUTOCOMPLETE, COMMAND_QUESTION } from './templates';
// Last generated completion storage
let lastCompletion = "";

/**
 * Notification with progress bar
 * @param progressText 
 * @param prompt 
 * @returns 
 */
async function generateWithProgress(progressText: string, prompt: string) {
    let result = '';
    await vscode.window.withProgress({
        location: vscode.ProgressLocation.Notification,
        cancellable: true,
        title: progressText
    }, async (progress) => {
        progress.report({ increment: 0 });
        result = await generate(prompt);
        progress.report({ increment: 100 });
    });
    return result;
}

/**
 * Calls KoboldCPP generate with autocomplete template
 * @param editor 
 */
export async function autocomplete(editor: vscode.TextEditor) {
    const params = vscode.workspace.getConfiguration('koboldcomplete');
    const model = params.whichModel;
    let template = getTemplate(model, COMMAND_AUTOCOMPLETE);

    // Get props and code from editor
    const currentLanguage = editor.document.languageId;
    const currentLineNumber = editor.selection.active.line;
    const endOfTheLastLine = editor.document.lineAt(currentLineNumber).range.end;
    let starLineNumber = 0;
    if (currentLineNumber > params.code_buffer) {
        starLineNumber = currentLineNumber - params.code_buffer;
    }
    const code = editor.document.getText(
        new vscode.Range(
            new vscode.Position(starLineNumber, 0),
            endOfTheLastLine
        )
    );

    // Fill template
    template = template.replaceAll('${currentLanguage}', currentLanguage);
    const prompt = template.replaceAll('${code}', code);

    let autocomplete = await generateWithProgress('KoboldCPP autocomplete, wait a sec', prompt);
    // Remove spaces around autocomplete
    autocomplete = autocomplete.trim();
    // Trying to remove comments from start of the code
    autocomplete = autocomplete.replace(/(.*?`)/i, '`');
    // Removing markdown
    autocomplete = autocomplete.replace(/(```.*)|(```*)/gi, '').trim();
    // Remove repeated code from autocomplete
    autocomplete = autocomplete.replace(code, '');

    if (params.experimentalInlineCompletion) {
        // Trigger inline completion
        lastCompletion = autocomplete;
        vscode.commands.executeCommand('editor.action.inlineSuggest.hide');
        vscode.commands.executeCommand('editor.action.inlineSuggest.trigger');
    } else {
        // Write code to current cursor position
        editor.edit((edit) => {
            edit.insert(endOfTheLastLine, autocomplete);
        });
    }
}

/**
 * Calls KoboldCPP generate with question template
 * @param editor 
 */
export async function question(editor: vscode.TextEditor) {
    const params = vscode.workspace.getConfiguration('koboldcomplete');
    const model = params.whichModel;
    let template = getTemplate(model, COMMAND_QUESTION);
    const { lineNumber, selectedText } = getSelection(editor);

    // Fill template and generate response
    template = template.replaceAll('${question}', selectedText);
    let response = await generateWithProgress('KoboldCPP question processing, wait a sec', template);

    // Paste answer below
    pasteTextBelow(editor, lineNumber, response.trim());
}

/**
 * Calls KoboldCPP generate without template
 * @param editor 
 */
export async function bare(editor: vscode.TextEditor) {
    const { lineNumber, selectedText } = getSelection(editor);

    // Generate response
    let response = await generateWithProgress('KoboldCPP question processing, wait a sec', selectedText);

    // Paste answer below
    pasteTextBelow(editor, lineNumber, response.trim());
}

/**
 * Stops generation by quering KoboldCPP api
 */
export async function stop() {
    abort();
    vscode.window.showInformationMessage('KoboldComplete: Generation stopped: ' + getKey());
}

/**
 * Deletes last saved completion for inlineCompletionProvider
 */
export function deleteLastCompletion() {
    lastCompletion = "";
}

/**
 * Returns last saved completion for inlineCompletionProvider
 */
export function getLastCompletion() {
    return lastCompletion;
}