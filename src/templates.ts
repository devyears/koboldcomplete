import * as vscode from 'vscode';
export const COMMAND_AUTOCOMPLETE = 'autocomplete';
export const COMMAND_QUESTION = 'question';

/**
 * Get template for command
 * @param modelName 
 * @param command 
 * @returns template: string
 */
export default function getTemplate(modelName: string, command: string): string {
    const params = vscode.workspace.getConfiguration('koboldcomplete');
    const commandFullName = `${command}Template${modelName}`;
    let template = params?.[commandFullName];
    if (!template) {
        console.error('No template: check if this command exists: ' + commandFullName);
        template = '';
    }

    return template;
}