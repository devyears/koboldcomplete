# KoboldComplete VSCode Extension
## Description
 - This is an extension that allows users to generate code/text using KoboldCPP.
 - Llama and Mistral-7B-Instruct are available, you need to change whichModel setting to use llama tho.
 - The extension provides several commands to generate code, including Autocomplete, Question, and Bare request without template.
 - It also includes snippets for bare KoboldCPP requests.
## Kudos for the idea to Reddit user Phial
 - His repository [Phiality KoboldConnect VSCODE](https://gitlab.com/phiality/koboldconnect-vscode)
 - By now, I used KoboldComplete for generating markdown and some python code. Works fine for me.
## Available commands
 - `KoboldComplete: Stop generating`
   - Stops Autocomplete, Question and Bare request commands
 - `KoboldComplete: Autocomplete`
   - Applies template(see autocompleteTemplate setting)
   - Sends code(see code_buffer setting) on the left and above the cursor to the KoboldCPP
 - `KoboldComplete: Question`
   - Applies template(see questionTemplate setting)
   - Sends code on the left from the cursor or selection to the KoboldCPP
 - `KoboldComplete: Bare request without template`
   - Sends code on the left from the cursor or selection to the KoboldCPP
## Available snippets *(are useful for bare requests)*
 - `mistral`: Mistral instruction snippet to tell NN what to do with prompt.
 - `llama`: LLaMA instruction snippet to tell NN what to do.
## Settings
![Settings screenshot](readme-images/settings.png)
## Installation
 - Install [KoboldCPP](https://github.com/LostRuins/koboldcpp)
    - Download [Mistral 7b-instruct model](https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.1-GGUF) in gguf format
      - If you want to use llama model you need to **set whichModel KoboldComplete setting in VSCode to Llama**
      - **You might also set max_length KoboldComplete setting in VSCode to lower number, for example 30**
    - For other models you might need to modify templates in extension settings
    - Run KoboldCPP
    - `Preset`: Use CuBLAS if you are on Nvidia and add some GPU layers; I use 99.
    - You might increase `Context size` up to 8k.
    - `Model`: Pick mistral-7b-instruct-v0.1.Q5_K_M file
 - [Get latest build of extension VSIX](https://gitlab.com/devyears/koboldcomplete/-/releases/)
    - Download koboldcomplete-x.x.x.vsix
    - Open VSCode
    - (Open Command Palette) `ctrl+shift+p` -> `Extensions: Install from VSIX...`
## How to use
 - Ask questions: `ctrl+shift+p` -> `KoboldComplete: Question`
    - Select text to send it to KoboldCPP, or it will send the current line
 - Autocomplete code: `ctrl+shift+p` -> `KoboldComplete: Autocomplete` or just `ctrl+m`
 - Feel free to add your keybinds to this actions
 - You might need to customize some parameters, for this, open settings `ctrl+,` and search for Koboldcomplete
## To build it from source
 - `git clone ${this repository link}`
 - `cd KoboldComplete`
 - `npm i`
 - `code .` or open vscode in KoboldComplete directory
 - Press `f5` to build it
 - You can test it in an opened window
 - If you want to generate VSIX file for installing in VSCode
   - `npm install -g @vscode/vsce`
   - `vsce package`
   - (Open Command Palette) `ctrl+shift+p` -> `Extensions: Install from VSIX...`
## Additional info
 - If you have any questions or issues with the extension, feel free to reach out to me on Reddit: **u/devyears**
 - Feel free to add pull requests and issues to this repository; contributions are welcome!
 - This is first version of the extension, so a lot of bugs are expected
 - This extension was made for fun and to test the concept of an offline neural network autocomplete plugin
 - The prompt template plays a huge role in generating code and answers, so I've spent a bunch of time fine-tuning